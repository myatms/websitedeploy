#!/bin/bash
apt update -y
apt install apache2 -y
apt install git -y
systemctl start apache2
cd /var/www/
git clone https://github.com/myatms/code.git
mv code/index.html html/index.html
systemctl restart apache2
